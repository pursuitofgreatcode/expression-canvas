#!/bin/bash

mkdir -p build
mkdir -p .tmp

# Remove only the files that have been deleted
echo && echo "$ Remove deleted files"
$(cd build && find . -not -path *\/.gitkeep > ../.tmp/blueprint-build)
$(cd src && find . > ../.tmp/blueprint-src)
diff .tmp/blueprint-build .tmp/blueprint-src | egrep "^< .*" | awk '{ b="./build/"$2; print b }' | xargs rm -rf 2>/dev/null

# Rebuild the files that remain
echo && echo "$ rsync non-js files (Skip vendor)"
rsync --recursive --info=progress -a --exclude '*.js' src/ build/
echo && echo "$ rsync ui/public/vendor files"
rsync --recursive --info=progress -a src/ui/public/vendor/ build/ui/public/vendor/
echo && echo "$ remove-types.sh"
./remove-types.sh
echo && echo "$ cd build/services/isolated-container && npm i"
cd build/services/isolated-container && npm i
echo "Done"

