
const { io } = require('socket.io-client')
const { spawn } = require('child_process')
const path = require('path')

const servicePath = path.join(__dirname, '../../services/isolated-container')

class Docker {

  constructor(port) {
    this.port = port
    this.client = null
    this.ons = []
  }

  async start() {
    // Cleanup living and dead containers on this port
    await this.stop()

    // Launch container
    const args = [
      'docker',
      'run',
      '--name',
      `auto_${this.port}`,
      '-v',
      `${servicePath}:/opt/isolated-container`,
      '-p',
      `127.0.0.1:${this.port}:3000/tcp`,
      'node:14.17.3',
      'node',
      '/opt/isolated-container/main.js'
    ]
    if (process.env.NODE_ENV !== 'production') {
      args.unshift('sudo')
    }

    const cmd = args.shift()
    await this._spawn(cmd, args)
    this.client = io(`ws://localhost:${this.port}/`)
    this.ons.map((on) => {
      this.client.on(on.name, on.callback)
    })
  }

  async stop() {
    try {
      await this._kill()
      await this._rm()
      this.client.disconnect()
      this.client = null
    } catch (err) {}
  }

  emit(...args) {
    if (!this.client) {
      return
    }

    this.client.emit(...args)
  }

  on(name, callback) {
    this.ons.push({
      name,
      callback
    })

    if (this.client) {
      this.client.on(name, callback)
    }
  }

  _kill() {
    const args = [
      'docker',
      'kill',
      `auto_${this.port}`
    ]
    if (process.env.NODE_ENV !== 'production') {
      args.unshift('sudo')
    }

    const cmd = args.shift()
    return this._exec(cmd, args)
  }

  _rm() {
    const args = [
      'docker',
      'rm',
      `auto_${this.port}`
    ]
    if (process.env.NODE_ENV !== 'production') {
      args.unshift('sudo')
    }

    const cmd = args.shift()
    return this._exec(cmd, args)
  }

  _spawn(cmd, args) {
    const myPort = this.port

    return new Promise((resolve, reject) => {
      const thread = spawn(cmd, args)
      thread.stdout.on('data', (d) => console.log(`vm[${myPort}] stdout`, d.toString('utf8')))
      thread.stderr.on('data', (d) => console.log(`vm[${myPort}] stderr`, d.toString('utf8')))
      thread.on('close', () => {
        console.log(`vm[${myPort}] close`)
      })

      resolve()
    })
  }

  _exec(cmd, args) {
    const myPort = this.port

    return new Promise((resolve, reject) => {
      const thread = spawn(cmd, args)
      thread.stdout.on('data', (d) => console.log(`vm[${myPort}] stdout`, d.toString('utf8')))
      thread.stderr.on('data', (d) => console.log(`vm[${myPort}] stderr`, d.toString('utf8')))
      thread.on('close', () => {
        console.log(`vm[${myPort}] close`)
        resolve()
      })
    })
  }

}

module.exports = Docker
