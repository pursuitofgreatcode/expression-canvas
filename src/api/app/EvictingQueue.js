class EvictingQueue {

  constructor(channel, onTimeout) {
    this._channel = channel
    this._onTimeout = onTimeout

    this._queue = null
    this._isProcessing = false
    this._TIMEOUT_MS = 250
    this._PING_TIMEOUT_MS = 50
    this._pingTimer = null
  }

  setDocker(docker) {
    this._docker = docker
    this._docker.on(`${this._channel}_pong`, () => {
      this._isProcessing = false
      clearTimeout(this._pingTimer)
      this.tryProcess()
    })
  }

  tryProcess() {
    if (!this._docker) {
      console.log('skipping run, docker instance missing')
      return
    }

    if (this._isProcessing) {
      console.log('skipping run, already processing')
      return
    }

    if (!this._queue) {
      console.log('skipping run, nothing in queue')
      return
    }

    this._isProcessing = true
    console.log('processing', this._queue)
    this._docker.emit(this._channel, ...this._queue)
    this._queue = null

    const self = this
    setTimeout(() => {
      this._docker.emit(`${this._channel}_ping`)
      self._pingTimer = setTimeout(() => {
        console.log('ping timeout')
        self._isProcessing = false
        self._onTimeout()
      }, self._PING_TIMEOUT_MS)
    }, this._TIMEOUT_MS)
  }

  queue(...args) {
    this._queue = args
  }

}

module.exports = EvictingQueue
