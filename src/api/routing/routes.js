// @flow

const path = require('path')
const handlebars = require(path.join(__dirname, '../middleware/handlebars'))

function home(req, res) {
  res.render("home", {
    title: "Home",
  });
}

function load(options: Object) {
  const { app, hbs } = options

  app.get("/", handlebars.exposeTemplates({app, hbs}), home);
}

exports.load = load
