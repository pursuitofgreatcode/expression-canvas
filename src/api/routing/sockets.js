// @flow

const path = require('path')
const Docker = require(path.join(__dirname, '../app/Docker'))
const EvictingQueue = require(path.join(__dirname, '../app/EvictingQueue'))

let uniquePort = 23000

async function onConnection(browser: Object) {
  console.log('A user connected')

  let fileSaveQueue = new EvictingQueue('file.save', _swapDockerInstance)

  function _createDockerInstance(port) {
    const docker = new Docker(port)

    // All `docker.on` calls must happen in here
    fileSaveQueue.setDocker(docker)
    return docker
  }

  async function _swapDockerInstance() {
    try {
      const newDocker = _createDockerInstance(docker.port)
      await docker.stop()
      await newDocker.start()
      docker = newDocker

      fileSaveQueue.tryProcess()
    } catch (err) {
      console.log('Error timing out Docker instance', err)
    }
  }

  let docker = _createDockerInstance(uniquePort)
  uniquePort++ // This must increment immediately

  await docker.start()

  browser.on('disconnect', function () {
    console.log('A user disconnected')

    docker
      .stop()
      .then(() => console.log('Docker stopped'))
      .catch(() => console.log('Docker error stopping'))
  })

  browser.on('file.save', (filename, contents) => {
    console.log('file.save', filename, contents)

    fileSaveQueue.queue(filename, contents)
    fileSaveQueue.tryProcess()
  })

}

exports.onConnection = onConnection
