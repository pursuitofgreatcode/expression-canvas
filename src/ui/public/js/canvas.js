// @flow

type CtxFn = { fn: string, args: Array<any>, type: 'fn' }
type CtxSet = { set: string, value: any, type: 'set' }

export class Canvas {

  _canvas: HTMLCanvasElement
  _ctx: Object
  _objects: { [name: string] : Array<CtxFn | CtxSet> }

  constructor(opts: { canvas: HTMLCanvasElement }) {
    this._canvas = opts.canvas || document.createElement('canvas')
    this._ctx = this._canvas.getContext('2d')
    this._objects = {}
  }

  _draw() {
    this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height)

    for (const name in this._objects) {
      for (const command of this._objects[name]) {
        if (command.type === 'fn') {
          this._ctx[command.fn](...command.args)
        } else {
          this._ctx[command.set] = command.value
        }
      }
    }
  }

  add(name: string, commands: Array<CtxFn | CtxSet>) {
    this._objects[name] = commands
    this._draw()
  }

  refresh() {
    this._drawGrid()
    this._draw()
  }

  _drawGrid() {
    const width = 100
    const height = 19

    let commands = [{
      set: 'strokeStyle',
      value: 'rgb(232,232,232)',
      type: 'set'
    }, {
      set: 'lineWidth',
      value: '1',
      type: 'set'
    }]

    for (let x = width - 0.5; x < this._canvas.width; x += width) {
      commands.push({
        fn: 'moveTo',
        args: [x, 0],
        type: 'fn'
      }, {
        fn: 'lineTo',
        args: [x, this._canvas.height],
        type: 'fn'
      }, {
        fn: 'stroke',
        args: [],
        type: 'fn'
      })
    }

    for (let y = height - 0.5; y < this._canvas.height; y += height) {
      commands.push({
        fn: 'moveTo',
        args: [0, y],
        type: 'fn'
      }, {
        fn: 'lineTo',
        args: [this._canvas.width, y],
        type: 'fn'
      }, {
        fn: 'stroke',
        args: [],
        type: 'fn'
      })
    }

    this.add('__grid', commands)
  }

}